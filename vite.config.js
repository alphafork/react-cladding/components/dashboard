import { resolve } from 'path';
import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';
import { peerDependencies } from './package.json';

// https://vitejs.dev/config/
export default defineConfig({
  build: {
    lib: {
      entry: resolve(__dirname, 'src/lib/main.jsx'),
      formats: ['es'],
      fileName: '@ims/dashboard',
    },
    rollupOptions: {
      external: Object.keys(peerDependencies),
    },
  },
  plugins: [react()],
});
