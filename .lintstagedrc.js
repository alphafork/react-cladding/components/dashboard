export default {
  'src/**/*.js?(x)': [
    'eslint --cache --fix',
  ],
};
