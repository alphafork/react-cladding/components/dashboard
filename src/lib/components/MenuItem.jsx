import React from 'react';

function MenuItem({ active, icon, label }) {
  return (
    <li className={active ? 'active-menutiem' : 'menuitem'}>
      <a className="p-ripple" href="/">
        <i className={`layout-menuitem-icon pi pi-fw pi-${icon}`} />
        <span className="layout-menuitem-text">{ label }</span>
      </a>
    </li>
  );
}

export default MenuItem;
