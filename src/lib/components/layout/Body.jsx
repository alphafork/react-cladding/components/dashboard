import React from 'react';

function Body({ children }) {
  return (
    <div className="layout-main">
      <div className="grid">
        { children }
      </div>
    </div>
  );
}

export default Body;
