import TopBar from './TopBar';
import SideBar from './SideBar';
import Container from './Container';
import Body from './Body';
import Footer from './Footer';

export {
  TopBar,
  SideBar,
  Body,
  Container,
  Footer,
};
