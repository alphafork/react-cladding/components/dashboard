import React from 'react';
import MenuItem from '../MenuItem';

function SideBar({ children }) {
  return (
    <div className="layout-sidebar">
      <div className="layout-menu">
        <ul>
          { children && children.map((item) => (
            <MenuItem
              key={item.id}
              active={item.active}
              icon={item.icon}
              label={item.label}
            />
          ))}
        </ul>
      </div>
    </div>
  );
}

export default SideBar;
