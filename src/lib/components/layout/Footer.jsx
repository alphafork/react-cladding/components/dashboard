import React from 'react';

function Footer({ text }) {
  return (
    <div className="layout-footer">
      <span className="font-medium ml-2">
        { text
          ?? (
          <div>
            Powered by&nbsp;
            <a href="https://alphafork.com" target="_blank" rel="noreferrer">
              Alpha Fork Technologies
            </a>
          </div>
          )}
      </span>
    </div>
  );
}

export default Footer;
