import React from 'react';

function TopBar({ children }) {
  return (
    <div className="layout-topbar">
      { children }
    </div>
  );
}

export default TopBar;
