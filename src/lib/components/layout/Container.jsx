import React from 'react';

function Container({ children }) {
  return (
    <div className="layout-main-container">
      { children }
    </div>
  );
}

export default Container;
