# Credits

The sass files for dashboard layout are taken from the
[sakai-react](https://www.primefaces.org/sakai-react) template offered for free
by [PrimeReact](https://primereact.org/templates).

See the original sass files here:
https://github.com/primefaces/sakai-react/tree/master/sakai-js/styles/layout
