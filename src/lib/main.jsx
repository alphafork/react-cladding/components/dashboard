import React from 'react';
import {
  TopBar, SideBar, Container, Body, Footer,
} from './components/layout';
import 'primereact/resources/primereact.min.css';
import 'primereact/resources/themes/lara-light-indigo/theme.css';
import 'primeicons/primeicons.css';
import 'primeflex/primeflex.css';
import './assets/sass/layout.scss';

function Dashboard({ children }) {
  return (
    <div className="layout-wrapper layout-theme-light layout-static">
      { children
      ?? (
      <>
        <TopBar />
        <SideBar />
        <Container>
          <Body />
          <Footer />
        </Container>
      </>
      )}
    </div>
  );
}

export default Dashboard;
export {
  TopBar, SideBar, Container, Body, Footer,
};
